import pygame
from pygame.locals import *
from numpy import ndarray

width, height = 800, 600
screen = pygame.display.set_mode((width, height))
door = pygame.image.load("resources/images/treasure.png")
banks = pygame.image.load("resources/images/bank.png")


class Player:
    def __init__(self, pos):
        self.image = pygame.image.load("resources/images/pirate.png")
        self.pos = pos
        self.score = 0
        self.checkpoint = [False, False, False, False, False]
        self.time = 0


class MovingObstacle:
    def __init__(self, pos, speed):
        self.image = pygame.image.load("resources/images/shark.png")
        self.pos = pos
        self.initial_speed = speed
        self.increment_speed = speed * 2
        self.direction = "R"


class StationaryObstacle:
    def __init__(self):
        self.image = pygame.image.load("resources/images/octopus.png")
        self.pos = [0, 0]  # To set some initial position, which is changed later in code


inc = ((height - (banks.get_height() * 6)) // 5) + banks.get_height()  # Height of a bank section and a river section combined
player1 = Player([0, 0])  # To set some initial position
player1.pos[1] = height - player1.image.get_height()  # Player1's original postion is bottom left of screen
player2 = Player([0, 0])  # To set some initial position
shark = [MovingObstacle([1, ((inc * 4) + banks.get_height() + 2.5)], 0.4),  # Shark1 bottom most river
         MovingObstacle([1, ((inc * 2) + banks.get_height() + 2.5)], 1),  # Shark2 in middle river
         MovingObstacle([1, (banks.get_height() + 2.5)], 0.2)]  # Shark3 in top most river
octopus = [StationaryObstacle(),
           StationaryObstacle(),
           StationaryObstacle(),
           StationaryObstacle()]
octopus[0].pos = [(width // 3), (inc + banks.get_height())]  # Declare postion of octopus1
octopus[1].pos = [(width - octopus[1].image.get_width()), (inc + banks.get_height())]  # Declare postion of octopus2
octopus[2].pos = [0, ((inc * 3) + banks.get_height())]  # Declare postion of octopus3
octopus[3].pos = [((width // 3) * 2), ((inc * 3) + banks.get_height())]  # Declare postion of octopus4
rounds = 0
isplayer1 = True  # To keep record of whose turn it is
keys = [False, False, False, False, False, False, False, False]
deadflag1 = False  # To check if player1 dies in a round
deadflag2 = False  # To check if player2 dies in a round


def set_screen(no):
    timer = pygame.time.get_ticks()  # Begin timer
    while (pygame.time.get_ticks() - timer) < 2500:  # Run loop for 2.5sec
        screen.fill(0)
        font = pygame.font.Font(None, 32)
        starttext = font.render(("ROUND " + str(no)),\
                                True,\
                                (255, 255, 255),\
                                (0, 0, 0))
        textRect = starttext.get_rect()
        textRect.center = [(width // 2), (height // 2)]
        screen.blit(starttext, textRect)
        pygame.display.flip()


def initialise_screen():
    screen.fill((156, 211, 219))
    for i in range(0, height, inc):
        screen.blit(banks, (0, i))
    if isplayer1:
        screen.blit(player1.image, player1.pos)
        screen.blit(door, (width - door.get_width(), 0))
        font = pygame.font.Font(None, 24)
        scoretext = font.render(("Score: " + str(player1.score)),\
                                True,\
                                (0, 0, 0))
        textRect = scoretext.get_rect()
        textRect.bottomright = [(width - 5), (height - 5)]  # Show the score in the bottom right corner of the screen
        screen.blit(scoretext, textRect)
    else:
        screen.blit(player2.image, player2.pos)
        screen.blit(door, (0, (height - door.get_height())))
        font = pygame.font.Font(None, 24)
        scoretext = font.render(("Score: " + str(player2.score)),\
                                True,\
                                (0, 0, 0))
        textRect = scoretext.get_rect()
        textRect.topleft = [0, 0]  # Show the score in the top left corner of the screen
        screen.blit(scoretext, textRect)
    for i in range(3):
        screen.blit(shark[i].image, shark[i].pos)
    for i in range(4):
        screen.blit(octopus[i].image, octopus[i].pos)
    pygame.display.flip()


def show_turn(no):
    timer = pygame.time.get_ticks()  # Begin timer
    while (pygame.time.get_ticks() - timer) < 1000:  # Run loop for 1sec
        screen.fill((0, 0, 128))
        font = pygame.font.Font(None, 32)
        starttext = font.render(("Player " + str(no)),\
                                True,\
                                (0, 255, 0),\
                                (0, 0, 128))
        textRect = starttext.get_rect()
        textRect.center = [(width // 2), (height // 2)]  # In the center of the screen
        screen.blit(starttext, textRect)
        pygame.display.flip()


def init_shark():
    if isplayer1:
        shark[0].pos = [1, ((inc * 4) + banks.get_height() + 2.5)]  # Sharks begin from the left side
        shark[1].pos = [1, ((inc * 2) + banks.get_height() + 2.5)]  # Sharks begin from the left side
        shark[2].pos = [1, (banks.get_height() + 2.5)]  # Sharks begin from the left side
        for i in range(3):
            if shark[i].direction == "L":  # Initially all sharks should face right
                shark[i].direction = "R"
                shark[i].image = pygame.transform.flip(shark[i].image,\
                                                       True,\
                                                       False)
    else:
        shark[0].pos = [(width - shark[0].image.get_width() - 1),\
                        ((inc * 4) + banks.get_height() + 2.5)]  # Sharks begin from right side
        shark[1].pos = [(width - shark[0].image.get_width() - 1),\
                        ((inc * 2) + banks.get_height() + 2.5)]  # Sharks begin from right side
        shark[2].pos = [(width - shark[0].image.get_width() - 1),\
                        (banks.get_height() + 2.5)]  # Sharks begin from right side
        for i in range(3):
            if shark[i].direction == "R":  # Initially all sharks should face left
                shark[i].direction = "L"
                shark[i].image = pygame.transform.flip(shark[i].image,\
                                                       True,\
                                                       False);


def winner():
    if player1.score > player2.score:
        return 1
    elif player2.score > player1.score:
        return 2
    else:
        if deadflag1 and deadflag2:  # If both are dead, the player lasting the longest wins
            if player2.time > player1.time:
                return 2
            elif player1.time > player2.time:
                return 1
            else:
                return 0
        else:  # If both players reach the end, the player who did it the fastest wins.
            if player2.time > player1.time:
                return 1
            elif player1.time > player2.time:
                return 2
            else:
                return 0


def end_round():
    win = winner()
    if win == 0:
        timer = pygame.time.get_ticks()
        while (pygame.time.get_ticks() - timer) < 2500:
            screen.fill(0)
            font = pygame.font.Font(None, 32)
            endtext = font.render(("THE GAME WAS A DRAW"),\
                                  True,\
                                  (255, 255, 255),\
                                  (0, 0, 0))
            textRect = endtext.get_rect()
            textRect.center = [(width // 2), (height // 2)]  # Center of the screen
            screen.blit(endtext, textRect)
            pygame.display.flip()
    else:
        timer = pygame.time.get_ticks()
        while (pygame.time.get_ticks() - timer) < 2500:
            screen.fill(0)
            font = pygame.font.Font(None, 32)
            endtext = font.render(("THE WINNER IS PLAYER " + str(win)),\
                                  True,\
                                  (255, 255, 255),\
                                  (0, 0, 0))
            textRect = endtext.get_rect()
            textRect.center = [(width // 2), (height // 2)]  # Center of the screen
            screen.blit(endtext, textRect)
            pygame.display.flip()
    global deadflag1
    global deadflag2
    if deadflag1 and deadflag2:  # Game ends only when both the players die in a round
        end_game()
    else:
        global rounds
        rounds += 1
        set_screen(rounds)
        show_turn(1)
        deadflag1 = False  # Both players are respawned at the beginning of a round
        deadflag2 = False  # Both players are respawned at the beginning of a round


def end_game():
    pygame.quit()
    exit(0)
