import pygame
from pygame.locals import *
import config

pygame.init()


def collision_move(p, i):
    flag = True
    if p == 1:
        if((config.player1.pos[0] > (config.shark[i].pos[0]\
                                     + config.shark[i].image.get_width()))\
           or (config.shark[i].pos[0] > (config.player1.pos[0]\
                                         + config.player1.image.get_width()))\
           or (config.player1.pos[1] > (config.shark[i].pos[1]\
                                        + config.shark[i].image.get_height()))\
           or (config.shark[i].pos[1] > (config.player1.pos[1]\
                                         + config.player1.image.get_height()))):
            flag = False  # Less conditions to test if objects not intersecting
        return flag
    else:
        if((config.player2.pos[0] > (config.shark[i].pos[0]\
                                     + config.shark[i].image.get_width()))\
           or (config.shark[i].pos[0] > (config.player2.pos[0]\
                                         + config.player2.image.get_width()))\
           or (config.player2.pos[1] > (config.shark[i].pos[1]\
                                        + config.shark[i].image.get_height()))\
           or (config.shark[i].pos[1] > (config.player2.pos[1]\
                                         + config.player2.image.get_height()))):
            flag = False  # Less conditions to test if objects not intersecting
        return flag


def collision_stat(p, i):
    flag = True
    if p == 1:
        if((config.player1.pos[0] > (config.octopus[i].pos[0]\
                                     + config.octopus[i].image.get_width()))\
           or (config.octopus[i].pos[0] > (config.player1.pos[0]\
                                           + config.player1.image.get_width()))\
           or (config.player1.pos[1] > (config.octopus[i].pos[1]\
                                        + config.octopus[i].image.get_height()))\
           or (config.octopus[i].pos[1] > (config.player1.pos[1]\
                                           + config.player1.image.get_height()))):
            flag = False  # Less conditions to test if objects not intersecting
        return flag
    else:
        if((config.player2.pos[0] > (config.octopus[i].pos[0]\
                                     + config.octopus[i].image.get_width()))\
           or (config.octopus[i].pos[0] > (config.player2.pos[0]\
                                           + config.player2.image.get_width()))\
           or (config.player2.pos[1] > (config.octopus[i].pos[1]\
                                        + config.octopus[i].image.get_height()))\
           or (config.octopus[i].pos[1] > (config.player2.pos[1]\
                                           + config.player2.image.get_height()))):
            flag = False  # Less conditions to test if objects not intersecting
        return flag


def is_dead():
    if config.isplayer1:
        for i in range(3):
            if collision_move(1, i):
                config.deadflag1 = True
                return True
        for i in range(4):
            if collision_stat(1, i):
                config.deadflag1 = True
                return True
    else:
        for i in range(3):
            if collision_move(2, i):
                config.deadflag2 = True
                return True
        for i in range(4):
            if collision_stat(2, i):
                config.deadflag2 = True
                return True


def is_complete():
    if config.isplayer1:
        if(((config.player1.pos[0] + config.player1.image.get_width())\
            < (config.width - config.door.get_width()))\
           or config.player1.pos[1] > config.door.get_height()):
            return False
        else:
            config.player1.score += 20
            return True  # Intersection with exit door signifies end of game
    else:
        if((config.player2.pos[0] > config.door.get_width()\
           or (config.player2.pos[1] + config.player2.image.get_height()))\
              < (config.height - config.door.get_height())):
            return False
        else:
            config.player2.score += 20
            return True  # Intersection with exit door signifies end of game


def update_keys(event_type, event_key):
    if event_type == pygame.KEYDOWN:
        if event_key == K_UP:
            config.keys[0] = True
        elif event_key == K_LEFT:
            config.keys[1] = True
        elif event_key == K_DOWN:
            config.keys[2] = True
        elif event_key == K_RIGHT:
            config.keys[3] = True
        elif event_key == K_w:
            config.keys[4] = True
        elif event_key == K_a:
            config.keys[5] = True
        elif event_key == K_s:
            config.keys[6] = True
        elif event_key == K_d:
            config.keys[7] = True
    if event_type == pygame.KEYUP:
        if event_key == K_UP:
            config.keys[0] = False
        elif event_key == K_LEFT:
            config.keys[1] = False
        elif event_key == K_DOWN:
            config.keys[2] = False
        elif event_key == K_RIGHT:
            config.keys[3] = False
        elif event_key == K_w:
            config.keys[4] = False
        elif event_key == K_a:
            config.keys[5] = False
        elif event_key == K_s:
            config.keys[6] = False
        elif event_key == K_d:
            config.keys[7] = False


def move_player():
    if config.isplayer1:
        if config.keys[0] and config.player1.pos[1] > 0:
            config.player1.pos[1] -= 0.6   # To make sure player does not go off screen
        elif config.keys[2] and ((config.player1.pos[1]\
                                  + config.player1.image.get_height())\
                                 < config.height):
            config.player1.pos[1] += 0.6  # To make sure player does not go off screen
        if config.keys[3] and ((config.player1.pos[0]\
                                + config.player1.image.get_width())\
                               < config.width):
            config.player1.pos[0] += 0.6  # To make sure player does not go off screen
        elif config.keys[1] and config.player1.pos[0] > 0:
            config.player1.pos[0] -= 0.6  # To make sure player does not go off screen
    else:
        if config.keys[4] and config.player2.pos[1] > 0:
            config.player2.pos[1] -= 0.6  # To make sure player does not go off screen
        elif config.keys[6] and ((config.player2.pos[1]\
                                  + config.player2.image.get_height())\
                                 < config.height):
            config.player2.pos[1] += 0.6  # To make sure player does not go off screen
        if config.keys[7] and ((config.player2.pos[0]\
                                + config.player2.image.get_width())\
                               < config.width):
            config.player2.pos[0] += 0.6  # To make sure player does not go off screen
        elif config.keys[5] and config.player2.pos[0] > 0:
            config.player2.pos[0] -= 0.6  # To make sure player does not go off screen


def update_score():
    if config.isplayer1:
        if(not config.player1.checkpoint[0]\
           and config.player1.pos[1] <= (config.height - config.inc)):
            config.player1.score += 10  # Reaching 2nd bank signifies crossing moving object
            config.player1.checkpoint[0] = True
        elif(not config.player1.checkpoint[1]\
             and config.player1.pos[1] <= (config.height - (config.inc * 2))):
            config.player1.score += 5  # Reaching 3rd bank signifies crossing statinary object
            config.player1.checkpoint[1] = True
        elif(not config.player1.checkpoint[2]\
             and config.player1.pos[1] <= (config.height - (config.inc * 3))):
            config.player1.score += 10  # Reaching 4th bank signifies crossing moving object
            config.player1.checkpoint[2] = True
        elif(not config.player1.checkpoint[3]\
             and config.player1.pos[1] <= (config.height - (config.inc * 4))):
            config.player1.score += 5  # Reaching 5th bank signifies crossing statinary object
            config.player1.checkpoint[3] = True
        elif not config.player1.checkpoint[4]:
            if(config.player1.pos[1] <= (config.height - (config.inc * 5))\
               or is_complete()):
                config.player1.score += 10  # Reaching 6th bank, or completing the round signifies crossing moving object
                config.player1.checkpoint[4] = True
    else:
        if(not config.player2.checkpoint[0]\
           and ((config.player2.pos[1] + config.player2.image.get_height())\
                >= config.inc)):
            config.player2.score += 10  # Reaching 2nd bank signifies crossing moving object
            config.player2.checkpoint[0] = True
        elif(not config.player2.checkpoint[1]\
             and ((config.player2.pos[1] + config.player2.image.get_height())\
                  >= (config.inc * 2))):
            config.player2.score += 5  # Reaching 3rd bank signifies crossing statinary object
            config.player2.checkpoint[1] = True
        elif(not config.player2.checkpoint[2]\
             and ((config.player2.pos[1] + config.player2.image.get_height())\
                  >= (config.inc * 3))):
            config.player2.score += 10  # Reaching 4th bank signifies crossing moving object
            config.player2.checkpoint[2] = True
        elif(not config.player2.checkpoint[3]\
             and ((config.player2.pos[1] + config.player2.image.get_height())\
                  >= (config.inc * 4))):
            config.player2.score += 5  # Reaching 5th bank signifies crossing statinary object
            config.player2.checkpoint[3] = True
        elif not config.player2.checkpoint[4]:
            if(((config.player2.pos[1] + config.player2.image.get_height())\
                >= (config.inc * 5))\
               or is_complete()):
                config.player2.score += 10  # Reaching 6th bank, or completing the round signifies crossing moving object
                config.player2.checkpoint[4] = True


def move_shark():
    for i in range(3):
        if((config.shark[i].pos[0] + config.shark[i].image.get_width())\
           >= config.width):
            config.shark[i].direction = "L"
            config.shark[i].image = pygame.transform.flip(\
                                    config.shark[i].image,\
                                    True,\
                                    False)  # To turn a shark
        elif config.shark[i].pos[0] <= 0:
            config.shark[i].direction = "R"
            config.shark[i].image = pygame.transform.flip(\
                                    config.shark[i].image,\
                                    True,\
                                    False)  # To turn a shark
        if config.shark[i].direction == "R":
            config.shark[i].pos[0] += config.shark[i].initial_speed\
                                      + (config.shark[i].increment_speed\
                                         * (config.rounds - 1))
        elif config.shark[i].direction == "L":
            config.shark[i].pos[0] -= config.shark[i].initial_speed\
                                      + (config.shark[i].increment_speed\
                                         * (config.rounds - 1))


while 1:
    if config.rounds == 0:
        config.rounds += 1
        config.set_screen(config.rounds)
        config.show_turn(1)
        timer = pygame.time.get_ticks()  # Begin a timer for the player
    config.initialise_screen()
    move_player()
    update_score()
    move_shark()
    if is_dead() or is_complete():  # To switch players and when required, rounds
        if config.isplayer1:
            config.player1.time += pygame.time.get_ticks() - timer  # Get time taken by player1
            config.player1.checkpoint = [False, False, False, False, False]  # Reset scoring checkpoints
            config.show_turn(2)
            timer = pygame.time.get_ticks()  # Begin a timer for player
            config.isplayer1 = False  # Change player
            config.player2.pos = [(config.width\
                                   - config.player2.image.get_width()), 0]  # Player 2 respawns in top right corner
            config.init_shark()
        else:
            config.player2.time += pygame.time.get_ticks() - timer  # Get time taken by player2
            config.player2.checkpoint = [False, False, False, False, False]  # Reset scoring checkpoints
            timer = pygame.time.get_ticks()  # Begin a timer for player
            config.isplayer1 = True  # Change player
            config.player1.pos = [0, (config.height\
                                      - config.player1.image.get_height())]  # Player 1 respawns in bottom left corner
            config.init_shark()
            config.end_round()
            config.player1.score = 0  # Reset score for next round
            config.player2.score = 0  # Reset score for next round
    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
            update_keys(event.type, event.key)
        if event.type == pygame.KEYUP:
            update_keys(event.type, event.key)
        if event.type == pygame.QUIT:
            config.end_game()
