# ISS Assignment 3

## PIRATE CROSSING

> This is a game in which 2 pirates practice to hunt treasure.
>
> To hunt treasure, a pirate must be capable of dodging all the obstacles that lie between, and **he must be fast!**.

> You can move Player 1 with the arrow keys(left, right, up, down).
>
> You can move player 2 with the keys W - up, A- left, S - down, D - right.
>
> Your goal is to reach the treasure chest on the other end of the screen.

> You will face 2 types of obstacles, a shark and an octopus.
>> Sharks are always moving horizontally from end to end. 
>>
>> Octopus never move.
> Collision with any of the obstacle results in the death of the player in that round.

> As long as atleast one player reaches their destination, both players keep competing in rounds.
>
> In each round, a player scores 10 points after crossing a shark and 5 points after crossing an octopus.
>
> After a round the scores of the 2 players are compared. In case they are the same, the winner is decided on the time taken to finish the round by each player. That is, **the faster player wins**.
>
> In case both the players die, **the player that survived for the longest wins**.
>
> The game ends only if both the players die in a round.

> But **BE AWARE**: The sharks tend to speed up as the rounds pass..

![Example of the gameplay.](pygame/resources/images/game_play.png)
